# Sharver

## Info

Kontrolery s� w /Controllers, Modele w /Models, a viewy w /Views.
W App_Data jest lokalna baza danych.
Na razie nie ruszajmy zwyk�ego logowania/rejestracji, �atwiej testowa�.
P�ki co, jest u�ytkownik oraz jego posty.

## Wa�ne kroki przed uruchomieniem
W konsole projektu wpisa�
Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r

## DB
Tools->NuGet Package Manager->Package Manager Console otwiera konsole.
### Tworzenie
W /Migrations jest config oraz migracje. Wystarczy w konsoli wpisa�
```
Update Database
```

### Update
Jak co� zmieniamy w modelach, to trzeba zaktualizowa� baz� danych.
W konsoli wpisujemy
```
Add-Migration Init
```
by doda� migracj�, a nast�pnie
```
Update Database
```
�eby zaktualizowa� BD.