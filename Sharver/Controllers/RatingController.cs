﻿using Microsoft.AspNet.Identity;
using Sharver.Models;
using static Sharver.Helpers.Rating;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System;


namespace Sharver.Controllers
{
    [Authorize]
    public class RatingController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("api/Rating/Post")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetPostVote(int id, bool direction)
        {
            string user = User.Identity.GetUserId();
            PostRating rating = await db.PostRating.FirstOrDefaultAsync(x => x.PostId == id && x.UserId == user);
            //Jeśli nie oddano jeszcze głosu, dodaj nowy
            if (rating == null)
            {
                rating = new PostRating
                {
                    UserId = user,
                    PostId = id,
                    Rating = direction
                };
                db.PostRating.Add(rating);
            }
            //inaczej zmodyfikuj istniejący głos
            else
            {
                rating.Rating = direction;
                db.Entry(rating).State = EntityState.Modified;
            }
            await db.SaveChangesAsync();
            // przelicz wynik
            int score = await CalculatePostRating(db, id);

            return Ok(score);
        }

        [Route("api/Rating/Post")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetPostRemove(int id)
        {
            string user = User.Identity.GetUserId();
            PostRating rating = await db.PostRating.FirstOrDefaultAsync(x => x.PostId == id && x.UserId == user);
            if (rating != null)
            {
                db.PostRating.Remove(rating);
                await db.SaveChangesAsync();
            }
            int score = await CalculatePostRating(db, id);
            return Ok(score);
        }

        [Route("api/Rating/Comment")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetCommentVote(int id, bool direction)
        {
            string user = User.Identity.GetUserId();
            CommentRating rating = await db.CommentRating.FirstOrDefaultAsync(x => x.CommentId == id && x.UserId == user);
            //Jeśli nie oddano jeszcze głosu, dodaj nowy
            if (rating == null)
            {
                rating = new CommentRating
                {
                    UserId = user,
                    CommentId = id,
                    Rating = direction
                };
                db.CommentRating.Add(rating);
            }
            //inaczej zmodyfikuj istniejący głos
            else
            {
                rating.Rating = direction;
                db.Entry(rating).State = EntityState.Modified;
            }
            await db.SaveChangesAsync();
            //przelicz ponownie wynik
            int score = await CalculateCommentRating(db, id);
            return Ok(score);
        }

        [Route("api/Rating/Comment")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetCommentRemove(int id)
        {
            string user = User.Identity.GetUserId();
            CommentRating rating = await db.CommentRating.FirstOrDefaultAsync(x => x.CommentId == id && x.UserId == user);
            if (rating != null)
            {
                db.CommentRating.Remove(rating);
                await db.SaveChangesAsync();
            }
            int score = await CalculateCommentRating(db, id);
            return Ok(score);
        }

        [Route("api/Rating/Post/Recalibrate")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetPostRecount(int id)
        {
            int score = await CalculatePostRating(db, id);
            return Ok(score);
        }

    }
}
