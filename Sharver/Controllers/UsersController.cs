﻿using Sharver.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sharver.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        

        public async Task<ActionResult> Posts(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await db.Users.Where(x => x.Id == id).Include(x => x.Posts).FirstAsync();
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.Posts = user.Posts.OrderBy(p => p.Score).ToList();
            ViewBag.User = user;
            return View("UserPosts");
        }
    }
}