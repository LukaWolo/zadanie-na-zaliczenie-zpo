﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sharver.Models;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Sharver.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

        // GET: Posts
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            var posts = db.Posts.Include(p => p.User);
            return View(await posts.ToListAsync());
        }

        // GET: Posts/Details/5
        [AllowAnonymous]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = await db.Posts
                .FirstOrDefaultAsync(p => p.Id == id);
                
                // query the database using EF here.
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName");
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create( PostDTO postDto)
        {
            Post post = new Post();
            if (ModelState.IsValid)
            {
                post.Title = postDto.Title;
                post.Flair = postDto.Flair;
                post.Type = postDto.Type;
                post.UserId = User.Identity.GetUserId();
                post.Created_at = DateTime.Now;
                if(post.Type == 2)
                {
                    string FileExtension = Path.GetExtension(postDto.ImageFile.FileName);
                    var hash = md5.ComputeHash(
                        Encoding.Default.GetBytes(
                            DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()
                            )
                        );
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < hash.Length; i++)
                    {
                        sb.Append(hash[i].ToString("X2"));
                    }
                    string FileName = sb.ToString() + FileExtension;
                    string UploadPath = Path.Combine(Server.MapPath("~/Uploads"), FileName);
                    postDto.ImageFile.SaveAs(UploadPath);
                    post.Content = FileName;
                }
                else
                {
                    post.Content = postDto.Content;
                }
                db.Posts.Add(post);
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = post.Id });
            }

            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", post.UserId);
            return View(postDto);
        }

        // GET: Posts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = await db.Posts.FindAsync(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if(post.UserId != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", post.UserId);
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Exclude = "Created_at")]Post post)
        {
            if (ModelState.IsValid)
            {
                var change = db.Posts.FirstOrDefault(p => p.Id == post.Id);
                if (change.UserId != User.Identity.GetUserId())
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                }
                change.Title = post.Title;
                change.Content = post.Content;
                change.Flair = post.Flair;
                change.Updated_at = DateTime.Now;
                db.Entry(change).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", post.UserId);
            return View(post);
        }

        // GET: Posts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = await db.Posts.FindAsync(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if (post.UserId != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Post post = await db.Posts.FindAsync(id);
            if (post.UserId != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            db.Posts.Remove(post);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public string UploadFile(HttpPostedFileBase file)
        {
            if (file.ContentLength <= 0)
                throw new Exception("Error while uploading");

            string fileName = Path.GetFileName(file.FileName);
            string path = Path.Combine(Server.MapPath("~/Uploads"), fileName);
            file.SaveAs(path);
            return "Successfuly uploaded";
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
