﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sharver.Startup))]
namespace Sharver
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
