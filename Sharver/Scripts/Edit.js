﻿
(function () {
    $(document).ready(function () {
        init();
    });
    function init() {
        $('input[name="Type"]').on('change', function () {
            var val = $(this).val();
            if (val == 0) {
                $('.file-group').hide();
                $('.file-group').find('input').val();
                $('.textarea-group').show();
            }
            if (val == 1) {
                $('.file-group').hide();
                $('.file-group').find('input').val();
                $('.textarea-group').show();
            }
            if (val == 2) {
                $('.file-group').show();
                $('.textarea-group').hide();
            }
            console.log(val);
        });

        $('#Type').trigger('change');
    }
})();
