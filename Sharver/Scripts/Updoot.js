﻿(function () {
    $(document).ready(function () {
        init();
    });

    function init() {
        $('body.logged').on('click', '.postSingle__doot',function () {
            var $post = $(this).closest('.postSingle');
            var valueUpdoot = $post.find('.postSingle__score').data('score');
            var id = $post.data('id');
            var direction = $(this).hasClass('postSingle__up');

            if ($(this).hasClass('active')) {

                if (direction)
                    valueUpdoot--;
                else
                    valueUpdoot++;

                $(this).removeClass('active');
                $.ajax({
                    url: '/api/Rating/Post', //remove rating
                    data: {
                        id: id
                    }
                })
            }
            else {
                $(this).addClass('active');
                if (direction) {
                    if ($post.find('.postSingle__down').hasClass('active'))
                        valueUpdoot += 2;
                    else
                        valueUpdoot += 1;
                    $post.find('.postSingle__down').removeClass('active');
                    
                }
                else {
                    if ($post.find('.postSingle__up').hasClass('active'))
                        valueUpdoot -= 2;
                    else
                        valueUpdoot -= 1;

                    $post.find('.postSingle__up').removeClass('active');
                }
                
                $.ajax({
                    url: '/api/Rating/Post', //add upvote/downvote
                    data: {
                        id: id,
                        direction: direction
                    }
                });
            }
            $post.find('.postSingle__score').data('score', valueUpdoot);
            $post.find('.postSingle__score').html(valueUpdoot);

        });

        $('body.logged').on('click', '.CommentSingle__doot', function () {
            var $post = $(this).closest('.CommentSingle');
            var valueUpdoot = $post.find('.CommentSingle__score').data('score');
            var id = $post.data('id');
            var direction = $(this).hasClass('CommentSingle__up');
            console.log($(this));
            if ($(this).hasClass('active')) {

                if (direction)
                    valueUpdoot--;
                else
                    valueUpdoot++;

                $(this).removeClass('active');
                $.ajax({
                    url: '/api/Rating/Comment', //remove rating
                    data: {
                        id: id
                    }
                })
            }
            else {
             
                $(this).addClass('active');
                if (direction) {
                    if ($post.find('.CommentSingle__down').hasClass('active'))
                        valueUpdoot += 2;
                    else
                        valueUpdoot += 1;
                    $post.find('.CommentSingle__down').removeClass('active');
                    
                }
                else {
                  
                    if ($post.find('.CommentSingle__up').hasClass('active'))
                        valueUpdoot -= 2;
                    else
                        valueUpdoot -= 1;

                    $post.find('.CommentSingle__up').removeClass('active');
                }
                
                $.ajax({
                    url: '/api/Rating/Comment', //add upvote/downvote
                    data: {
                        id: id,
                        direction: direction
                    }
                });
            }
            $post.find('.CommentSingle__score').data('score', valueUpdoot);
            $post.find('.CommentSingle__score').html(valueUpdoot);

        });
    }


})();