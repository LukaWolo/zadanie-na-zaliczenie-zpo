namespace Sharver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentRatings",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        CommentId = c.Int(nullable: false),
                        Rating = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.CommentId })
                .ForeignKey("dbo.Comments", t => t.CommentId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.CommentId);
            
            CreateTable(
                "dbo.PostRatings",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        PostId = c.Int(nullable: false),
                        Rating = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.PostId })
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.PostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostRatings", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CommentRatings", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PostRatings", "PostId", "dbo.Posts");
            DropForeignKey("dbo.CommentRatings", "CommentId", "dbo.Comments");
            DropIndex("dbo.PostRatings", new[] { "PostId" });
            DropIndex("dbo.PostRatings", new[] { "UserId" });
            DropIndex("dbo.CommentRatings", new[] { "CommentId" });
            DropIndex("dbo.CommentRatings", new[] { "UserId" });
            DropTable("dbo.PostRatings");
            DropTable("dbo.CommentRatings");
        }
    }
}
