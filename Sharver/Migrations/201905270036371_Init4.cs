namespace Sharver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CommentRatings", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.PostRatings", "PostId", "dbo.Posts");
            DropForeignKey("dbo.PostRatings", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.CommentRatings", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.CommentRatings", new[] { "CommentId" });
            DropIndex("dbo.CommentRatings", new[] { "User_Id" });
            DropIndex("dbo.PostRatings", new[] { "PostId" });
            DropIndex("dbo.PostRatings", new[] { "User_Id" });
            DropTable("dbo.CommentRatings");
            DropTable("dbo.PostRatings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PostRatings",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                        Rating = c.Boolean(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.PostId });
            
            CreateTable(
                "dbo.CommentRatings",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        CommentId = c.Int(nullable: false),
                        Rating = c.Boolean(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.CommentId });
            
            CreateIndex("dbo.PostRatings", "User_Id");
            CreateIndex("dbo.PostRatings", "PostId");
            CreateIndex("dbo.CommentRatings", "User_Id");
            CreateIndex("dbo.CommentRatings", "CommentId");
            AddForeignKey("dbo.CommentRatings", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.PostRatings", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.PostRatings", "PostId", "dbo.Posts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CommentRatings", "CommentId", "dbo.Comments", "Id", cascadeDelete: true);
        }
    }
}
