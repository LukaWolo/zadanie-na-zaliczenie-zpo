using Sharver.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Sharver.Helpers
{
    public static class Rating
    {
        public async static Task<int> CalculatePostRating(ApplicationDbContext db, int id)
        {
            Post post = await db.Posts.FirstAsync(p => p.Id == id);
            var score = await db.PostRating.Where(pr => pr.PostId == id).SumAsync(pr => (int?)(pr.Rating ? 1 : -1)) ?? 0;
            post.Score = score;
            db.Entry(post).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return post.Score;
        }

        public async static Task<int> CalculateCommentRating(ApplicationDbContext db, int id)
        {
            Comment post = await db.Comments.FirstAsync(p => p.Id == id);
            var score = await db.CommentRating.Where(pr => pr.CommentId == id).SumAsync(pr => (int?)(pr.Rating ? 1 : -1)) ?? 0;
            post.Score = score;
            db.Entry(post).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return post.Score;
        }
    }
}