using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Sharver.Models
{

    public class Comment
    {
        [Required]
        public int Id { get; set; }

        public int Score { get; set; } = 0;

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Created_at { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? Updated_at { get; set; }

        //Foreign
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        [InverseProperty("Children")]
        public virtual Comment Parent { get; set; }
        public virtual ICollection<Comment> Children { get; set; }


        public virtual ICollection<CommentRating> CommentRating { get; set; }
    }
}