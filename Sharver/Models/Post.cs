﻿using Sharver.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Sharver.Models
{

    public class Post
    {
        [Required]
        public int Id { get; set; }

        [DisplayName("Wynik")]
        public int Score { get; set; } = 0;

        [DisplayName("Tytuł")]
        [DataType(DataType.Text)]
        [Required]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }


        public string Flair { get; set; }

        //0 - text
        //1 - url
        //2 - file
        public Byte Type { get; set; } = 0;

        [DisplayName("Czas stworzenia")]
        [DataType(DataType.DateTime)]
        public DateTime Created_at { get; set; }

        [DisplayName("Czas ostatniej aktualizacji")]
        [DataType(DataType.DateTime)]
        public DateTime? Updated_at { get; set; }

        //Foreign
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<PostRating> PostRating { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        
    }

    public class PostDTO
    {
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        public string Flair { get; set; }
        public Byte Type { get; set; } = 0;
        public HttpPostedFileBase ImageFile { get; set; }
    }
}