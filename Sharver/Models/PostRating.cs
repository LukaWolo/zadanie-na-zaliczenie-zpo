using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Sharver.Models
{

    public class PostRating
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public int PostId { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Post Post { get; set; }

        //true: wgóręgłos; false: wdółgłos
        public bool Rating { get; set; }


    }
}