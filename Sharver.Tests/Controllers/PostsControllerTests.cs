﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sharver;
using Sharver.Controllers;
using Sharver.Models;

namespace Sharver.Tests.Controllers
{
    [TestClass]
    public class PostsControllerTest
    {
        [TestMethod]
        public async Task Index()
        {
            // Arrange
            PostsController controller = new PostsController();

            // Act
            ViewResult result = (await controller.Index()) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Details()
        {
            // Arrange
            PostsController controller = new PostsController();

            // Act
            ViewResult result = (await controller.Details(1)) as ViewResult;

            // Assert
            Assert.IsNotNull(result);

            //Act
            var model = (Post)result.ViewData.Model;

            // Assert
            Assert.AreEqual("Pierwszy wpis", model.Title);
        }
    }
}