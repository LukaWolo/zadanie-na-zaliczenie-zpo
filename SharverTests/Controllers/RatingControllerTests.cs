﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sharver;
using Sharver.Controllers;
using Sharver.Models;

namespace SharverTests.Controllers
{
    [TestClass]
    public class RatingControllerTests
    {
        [TestMethod]
        public async Task GetPostRemove()
        {
            // Arrange
            RatingController controller = new RatingController();

            // Act
            var result = (await controller.GetPostRecount(1)) as OkNegotiatedContentResult<int>;

            // Assert
            Assert.AreEqual(1, result.Content);
        }
    }
}
